<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Researcher</title> 
<%@ include file = "Header.jsp"%>
</head>
<body>

<table>

<tr>
<th>Id</th><th>Name</th><th>Last name</th><th>URL</th><th>Email</th>
</tr>
 <tr>
            	
				<td> ${ri.id}</td>
                <td>${ri.name}</td>
                <td>${ri.lastname}</td>
                <td><a href="${ri.scopusURL}">${ri.scopusURL}</a></td>
                <td>${ri.email}</td>
        </tr>
<c:forEach items="${publication}" var="pi">
 <tr>
            	<td> <a href="PublicationServlet?id=${pi.id}"> ${pi.id} </a></td>
        </tr>
</c:forEach>
</table>
       
<h2>Publications</h2>
<table>
<c:forEach items="${publications}" var="ri">
        <tr>
            <td><a href="PublicationServlet?id=${ri.id}">${ri.id}</a></td>
            </tr>
</c:forEach>
</table>

</body>
</html>